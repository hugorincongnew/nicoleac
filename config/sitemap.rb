Sitemap::Generator.instance.load :host => "nicoleac.com" do
  path :root, :priority => 1, :change_frequency => "daily"
  
  literal "/faq", :priority => 0.5, :change_frequency => "weekly"
  literal "/reglas", :priority => 0.5, :change_frequency => "weekly"  
  literal "/contact", :priority => 0.5, :change_frequency => "monthly"
  
  Publicacion.all.each do |p|
    literal "/publicaciones/#{p.slug}", :priority => 0.6, :change_frequency => "weekly"
  end  
  Equipo.all.each do |e|
    literal "/equipoa/#{e.id}/public", :priority => 0.6, :change_frequency => "weekly"
  end  
end