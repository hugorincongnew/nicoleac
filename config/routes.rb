Nicoleac::Application.routes.draw do

  root :to => 'home#index'
  #match '/', :to => redirect('/underconstruction/index.html') 
   
  match '/main' => "home#index"
   
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  match '/contact/create' => 'home#create_contact'
  match '/catalogo' => 'home#catalogo'
  match '/categoria/:id' => 'home#categoria'
  match '/producto/:id' => 'home#producto'
  
  get '/admin/contacts/:id' => 'admin/contacts#show'
end
