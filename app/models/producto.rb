class Producto < ActiveRecord::Base
  attr_accessible :activa, :categoria_id, :codigo, :disponible, :home, :imagen, :nota, :nombre, :detalle, :precio

  mount_uploader :imagen, ImageUploader  
  
  validates_presence_of :imagen, :categoria_id

  belongs_to :categoria 
  
  after_save :post_to_fb
  
  def post_to_fb
    #@graph = Koala::Facebook::API.new(ENV["fb_access_token"])
    #@graph.put_picture(self.full_url, {:message => "#{self.nombre}"})
  end  
  
  def watermark
    self.nota
  end  

  def full_url
    "http://www.nicoleac.com/#{imagen.url}"
  end  
  
  def auto_code
    list = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', 'ax','bx','cx','dx','ex','fx','gx','hx','ix','jx','kx','lx','mx','nx','ox','px','qx','rx','sx','tx','ux','vx','wx','xx','yx','zx']
    number = "%03d" % id.to_i
    "#{list[categoria_id.to_i]}#{number}".upcase
  end  
  
  def default_code
    (codigo.nil? or codigo == "")  ? auto_code : codigo
  end  
  
  def default_nombre
    (nombre.nil? or nombre == "") ? default_code.upcase : "#{nombre.capitalize} (#{default_code})"
  end  
  
  def precio_categoria
    categoria.precio
  end      
  
  def precio_default
    p = (precio.nil? or precio == "") ? precio_categoria : precio
    if !p.nil? and p != ""
      "#{p} BSF. c/u"
    else
      ""
    end    
  end  

  def producto_url
    "/producto/#{id}"
  end  

end
