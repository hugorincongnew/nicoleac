class Banner < ActiveRecord::Base
  attr_accessible :activa, :imagen

  mount_uploader :imagen, BannerUploader  

  def self.activos
    Banner.where(:activa => true)
  end
  
end
