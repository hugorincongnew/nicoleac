class Contact < ActiveRecord::Base
  attr_accessible :email, :mensaje, :nombre
  validates_presence_of :email, :mensaje, :nombre, :message => 'No puede estar en blanco'
end
