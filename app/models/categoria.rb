class Categoria < ActiveRecord::Base
  attr_accessible :nombre, :precio, :nota
  has_many :productos, :dependent => :delete_all
  
  validates_presence_of :nombre
  
  def name
    nombre
  end  
  
  def l_p_u
    p = productos.order('updated_at DESC').first
    p.nil? ? Time.now - 30.days : p.updated_at
  end  
  
  def ultimos_5
    productos.last(5).reverse
  end  
  
  def ultimos_7
    productos.last(7).reverse
  end    
  
  def last_product_update
    if !productos.order('updated_at desc').first.nil?
      productos.order('updated_at desc').first.created_at 
    else
      Time.now - 30.days
    end    
  end
    
end
