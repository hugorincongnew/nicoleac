ActiveAdmin.register Contact do     
  menu :label => "Mensajes"
  index do                             
    column :nombre 
    column :email     
    column :created_at       
    default_actions                   
  end                                             

  form do |f|                         
    f.inputs "Detalles Del Mensaje" do   
      f.input :nombre, :label => 'Nombre de contacto' 
      f.input :email, :label => 'Email'     
      f.input :mensaje, :label => 'Mensaje'        
    end                               
    f.actions                         
  end                                 
end                                   
