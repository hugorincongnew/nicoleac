ActiveAdmin.register Categoria do   
  config.batch_actions = true
    
  menu :label => "Categorias"
  index do                            
    column :nombre            
    column :nota
    column :precio                   
    default_actions                   
  end                                 

  filter :nombre                 

  form do |f|                         
    f.inputs "Detalles De Categoria" do       
      f.input :nombre, :label => 'Nombre'                 
      f.input :nota, :label => 'Descripcion de la categoria'       
      f.input :precio, :label => 'Precio'
    end                               
    f.actions                         
  end                                 
end                                   
