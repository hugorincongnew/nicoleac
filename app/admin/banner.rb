ActiveAdmin.register Banner do     
  menu :label => "Banners"
  index do     
    column :activa                             
    default_actions                   
  end                                             

  form do |f|                         
    f.inputs "Detalles Del Producto" do   
      f.input :activa, :label => 'Activar/Desactivar Imagen'                 
      f.input :imagen, :label => 'Imagen'                     
    end                               
    f.actions                         
  end                                 
end                                   
