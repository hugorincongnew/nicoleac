ActiveAdmin.register Producto do     
  menu :label => "Productos"

  index do                             
    column "Imagen" do |producto|
      "<img src='#{producto.imagen.mini}' width=50 />".html_safe
    end  
    column :nombre
    column :categoria        
    column :activa
    column :disponible        
    default_actions                   
  end                                             

  form do |f|                         
    f.inputs "Detalles Del Producto" do   
      f.input :activa, :label => 'Activar/Desactivar Imagen'   
      f.input :disponible, :label => 'Esta disponible?' 
      f.input :nombre, :label => 'Nombre'   
      f.input :precio, :label => 'Precio'      
      f.input :detalle, :label => 'Detalles'  
      f.input :nota, :label => "Logo", :as => :select, :collection => [["Arriba Izquierda", "watermark_ai.png"], ["Arriba Derecha", "watermark_ad.png"], ["Abajo Izquierda", "watermark_bi.png"], ["Abajo Derecha", "watermark_bd.png"]]       
      f.input :categoria, :label => 'Categoria'  
      f.input :codigo, :label => 'Codigo'  
      f.input :imagen, :label => 'Imagen'                      
    end                               
    f.actions                         
  end                                 
end                                   
