class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :preparar_contact
  def preparar_contact
    @contact2 = Contact.new
  end  
end
