class HomeController < ApplicationController
  layout 'main'
  
  def index
    @home = true
    @banner = Banner.activos
    @ultimas = Producto.last(10).reverse
  end
  
  def sort_by_product_update(categories)
    categories.sort { |x,y| y.last_product_update <=> x.last_product_update}
  end  
  
  def catalogo
    @categorias = Categoria.all
    @description = "Catalogo de productos"
  end  
  
  def categoria
    @categoria = Categoria.find(params[:id])
    @description = @categoria.nombre
  end  
  
  def producto
    @full_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}"
    @producto = Producto.find(params[:id])
    @image_path = @producto.imagen
    @description = @producto.nombre    
  end  
  
  def create_contact
    contact = Contact.new(params[:contact])
    if contact.save
      flash[:notice] = "#{params[:contact][:nombre].capitalize} tu mensaje fue enviado, pronto reciviras respuesta en tu correo '#{params[:contact][:email]}'."
      ContactMailer.contact_email(contact).deliver
      render :json => {:mensaje => '¡Gracias por contactarnos! Te contactaremos en las próximas horas.'}
    else
      flash[:notice] = contact.errors.map{|k,v| "#{k.capitalize}: #{v}"}.join(', ')
      render :json => {:error => 'Todos los campos son necesarios!'}
    end  
    
  end  
  
end
