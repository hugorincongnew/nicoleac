class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  def store_dir
    'public/uploads/images'
  end

  storage :file
  process :convert => 'jpg'
  process :resize_to_fill => [650, 650]
  process :watermark => "public/"
  
  version :mini do
    process :resize_to_fill => [100, 100, :north]
  end  
  
  version :home do
    process :resize_to_fill => [247, 247, :north]
  end  

  def filename
    "#{secure_token(10)}.#{file.extension}" if original_filename.present?
  end

  def watermark(path)   
    if model.watermark   
      path_to_file = "#{path}#{model.watermark}"
      manipulate! do |img|
        img = img.composite(MiniMagick::Image.open(path_to_file), "png") do |c|
          c.gravity "SouthEast"
          c.blend 99
        end
      end
    end  
  end
  
  protected
  def secure_token(length=16)
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.hex(length/2))
  end
end