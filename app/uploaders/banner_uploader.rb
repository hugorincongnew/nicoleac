class BannerUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  
  def store_dir
    'public/uploads/banners'
  end
  
  #include Cloudinary::CarrierWave
  storage :file
  process :convert => 'jpg'
  process :resize_to_fill => [1041, 381]

end