class ContactMailer < ActionMailer::Base
  
  def contact_email(contact)
    @contact2 = contact
    mail(from: 'info@nicoleac.com', to: 'ventas@nicoleac.com', subject: 'Pregunta desde nicoleac.com!')
  end
  
end
