class CreateProductos < ActiveRecord::Migration
  def change
    create_table :productos do |t|
      t.integer :categoria_id
      t.string :codigo
      t.string :imagen
      t.string :nota
      t.boolean :home, :null => false, :default => false
      t.boolean :activa, :null => false, :default => true
      t.boolean :disponible, :null => false, :default => true

      t.timestamps
    end
  end
end
