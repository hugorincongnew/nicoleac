class CreateCategoria < ActiveRecord::Migration
  def change
    create_table :categoria do |t|
      t.string :nombre
      t.float :precio, :default => 0.0

      t.timestamps
    end
  end
end
