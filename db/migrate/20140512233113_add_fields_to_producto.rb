class AddFieldsToProducto < ActiveRecord::Migration
  def change
    add_column :productos, :nombre, :string
    add_column :productos, :detalle, :text
    add_column :productos, :precio, :string  
  end
end
