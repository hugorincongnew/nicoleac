class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.boolean :activa
      t.string :imagen

      t.timestamps
    end
  end
end
