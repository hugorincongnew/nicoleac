class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :email
      t.text :mensaje
      t.string :nombre

      t.timestamps
    end
  end
end
